import React, { Component } from "react";

class Benefits extends Component {
  render() {
    return (
      <section id="service" className="blog-mf sect-pt4 route">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="title-box text-center">
                <h3
                  className="title-a"
                  style={{ fontFamily: "Indie Flower, cursive" }}
                >
                  TABL Digital Menu Benefits:
                </h3>
                <div className="line-mf" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="card card-blog">
                <div
                  className="card-head"
                  style={{ width: "50%", margin: "auto", marginTop: "10px" }}
                >
                  <img
                    src="./img/icons/restaurant-service.png"
                    alt=""
                    style={{
                      height: "150px",
                      width: "150px",
                      margin: "0 auto"
                    }}
                  />
                </div>

                <div className="card-body">
                  <h3 className="card-title">
                    <a href="#service">Faster Service</a>
                  </h3>
                  <p className="card-description" />
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card card-blog">
                <div
                  className="card-head"
                  style={{ width: "50%", margin: "auto", marginTop: "10px" }}
                >
                  <img
                    src="./img/icons/shots.png"
                    alt=""
                    style={{
                      height: "150px",
                      width: "150px",
                      margin: "0 auto"
                    }}
                  />
                </div>

                <div className="card-body">
                  <h3 className="card-title">
                    <a href="#service">More Sales</a>
                  </h3>
                  <p className="card-description" />
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card card-blog">
                <div
                  className="card-head"
                  style={{ width: "50%", margin: "auto", marginTop: "10px" }}
                >
                  <img
                    src="./img/icons/budget.png"
                    alt=""
                    style={{
                      height: "150px",
                      width: "150px",
                      margin: "0 auto"
                    }}
                  />
                </div>

                <div className="card-body">
                  <h3 className="card-title">
                    <a href="#service">Lower Costs</a>
                  </h3>
                  <p className="card-description" />
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="card card-blog">
                <div
                  className="card-head"
                  style={{ width: "50%", margin: "auto", marginTop: "10px" }}
                >
                  <img
                    src="./img/icons/notebook.png"
                    alt=""
                    style={{
                      height: "150px",
                      width: "150px",
                      margin: "0 auto"
                    }}
                  />
                </div>

                <div className="card-body">
                  <h3 className="card-title">
                    <a href="#service">No More Mistaken Orders</a>
                  </h3>
                  <p className="card-description" />
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card card-blog">
                <div
                  className="card-head"
                  style={{ width: "50%", margin: "auto", marginTop: "10px" }}
                >
                  <img
                    src="./img/icons/menu.png"
                    alt=""
                    style={{
                      height: "150px",
                      width: "150px",
                      margin: "0 auto"
                    }}
                  />
                </div>

                <div className="card-body">
                  <h3 className="card-title">
                    <a href="#service">No Need For Expensive Paper Menus</a>
                  </h3>
                  <p className="card-description" />
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card card-blog">
                <div
                  className="card-head"
                  style={{ width: "50%", margin: "auto", marginTop: "10px" }}
                >
                  <img
                    src="./img/icons/to-go-cup.png"
                    alt=""
                    style={{
                      height: "150px",
                      width: "150px",
                      margin: "0 auto"
                    }}
                  />
                </div>

                <div className="card-body">
                  <h3 className="card-title">
                    <a href="#service">Ability To Have Personalized Promos</a>
                  </h3>
                  <p className="card-description" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Benefits;
