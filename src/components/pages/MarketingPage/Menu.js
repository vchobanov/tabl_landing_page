import React, { Component } from "react";

class Menu extends Component {
  render() {
    return (
      <nav
        className="navbar navbar-b navbar-trans navbar-expand-md fixed-top"
        id="mainNav"
      >
        <div className="container">
          <a
            className="navbar-brand js-scroll"
            style={{ fontFamily: "Indie Flower, cursive" }}
            href="#page-top"
          >
            Tabl
          </a>
          <button
            className="navbar-toggler collapsed"
            type="button"
            data-toggle="collapse"
            data-target="#navbarDefault"
            aria-controls="navbarDefault"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span />
            <span />
            <span />
          </button>
          <div
            className="navbar-collapse collapse justify-content-end"
            id="navbarDefault"
          >
            <ul className="navbar-nav">
              <li className="nav-item">
                <a
                  className="nav-link js-scroll"
                  style={{ fontFamily: "Indie Flower, cursive" }}
                  href="#about"
                >
                  About
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link js-scroll"
                  style={{ fontFamily: "Indie Flower, cursive" }}
                  href="#service"
                >
                  Services
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link js-scroll"
                  style={{ fontFamily: "Indie Flower, cursive" }}
                  href="#partners"
                >
                  Partners
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link js-scroll"
                  style={{ fontFamily: "Indie Flower, cursive" }}
                  href="#contact"
                >
                  Contact
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Menu;
