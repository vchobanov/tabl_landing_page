import React, { Component } from "react";
import Location from "../Partners/Location";
import Menu from "./Menu";
import Benefits from "./Benefits";
import Banner from "./Banner";
import Footer from "./Footer";

class MarketingPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      locations: [
        {
          name: "Coffee Lab_br",
          url: "https://coffeebar.tabl.site"
        },
        {
          name: "Street Cafe",
          url: "http://streetcafe.tabl.site"
        }
      ]
    };
  }

  render() {
    return (
      <div>
        <Menu />
        <Banner />
        <Benefits />
        <section id="partners" className="blog-mf sect-pt4 route">
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <div className="title-box text-center">
                  <h3
                    className="title-a"
                    style={{"fontFamily": "Indie Flower, cursive"}}
                  >
                    Trusted by:
                  </h3>
                  <div className="line-mf" />
                </div>
              </div>
            </div>
            <div className="row">
              {this.state.locations.map(location => {
                return <Location name={location.name} url={location.url} key={location.name} />;
              })}
            </div>
          </div>
        </section>
   
       <Footer />

        <a href="#" className="back-to-top">
          <i className="fa fa-chevron-up" />
        </a>
        {/* <div id="preloader" /> */}
      </div>
    );
  }
}

export default MarketingPage;
