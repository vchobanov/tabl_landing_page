import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
        <section className="paralax-mf footer-paralax bg-image sect-mt4 route" style={{"backgroundImage": "url(img/bar2-bg.jpg)"}}>
        <div className="overlay-mf"></div>
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="contact-mf">
                <div id="contact" className="box-shadow-full">
                  <div className="row">
    
                    <div className="col-md-12">
                      <div className="title-box-2 pt-4 pt-md-0">
                        <h5 className="title-left">
                          Get in Touch
                        </h5>
                      </div>
                      <div className="more-info">
                        <ul className="list-ico">
                          <li><span className="ion-ios-location"></span> <a target="_blank" href="https://www.google.com/maps/place/1700+Studentski+grad,+Sofia/@42.6542645,23.3484032,17z/data=!3m1!4b1!4m5!3m4!1s0x40aa8422f4fe9815:0x227b1c46f061e819!8m2!3d42.6541867!4d23.3514271">Sofia, Studentski grad</a></li>
                          <li><span className="ion-ios-telephone"></span> <a href="tel:+359899982932">(+359) 899 982 932</a></li>
                          <li><span className="ion-ios-telephone"></span> <a href="tel:+359884014689">(+359) 884 014 689</a></li>
                          <li><span className="ion-email"></span> <a href="mailto:support@tabl.site">support@tabl.site</a></li>
                        </ul>
                      </div>
                      <div className="socials">
                        <ul>
                          <li><a href="https://www.facebook.com/Tabl-696521427410587/"><span className="ico-circle"><i className="ion-social-facebook"></i></span></a></li>
                          <li><a href="https://www.instagram.com/tabl.site/"><span className="ico-circle"><i className="ion-social-instagram"></i></span></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer>
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <div className="copyright-box">
                  <p className="copyright">&copy; Copyright <strong>Tabl</strong>. All Rights Reserved</p>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </section>
    );
  }
}

export default Footer;
