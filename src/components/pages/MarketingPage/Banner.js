import React, { Component } from "react";

class Banner extends Component {
  render() {
    return (
      <div
        id="home"
        className="intro route bg-image"
        style={{"backgroundImage": "url(img/bar-bg.jpg)"}}
      >
        <div className="overlay-itro" />
        <div className="intro-content display-table">
          <div className="table-cell">
            <div className="container">
              <h1
                className="intro-title mb-4"
                style={{"fontFamily": 'Indie Flower, cursive'}}
              >
                Digital menu system
              </h1>
              <p className="intro-subtitle">
                <span className="text-slider-items">
                  Personalized menu, Mobile application, Kitchen system
                </span>
                <strong className="text-slider" />
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Banner;
