import React, { Component } from "react";
import Location from "./Location";

class Partners extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      locations: [
        {
          name: "Coffee Lab_br",
          url: "https://coffeebar.tabl.site"
        },
        {
          name: "Street Cafe",
          url: "http://streetcafe.tabl.site"
        }
      ]
    };
  }

  render() {
    return (
      <div id="home" className="intro route bg-image" style={{"backgroundImage": "url(img/bar-bg.jpg)"}}>
      <section id="partners" className="blog-mf sect-pt4 route">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="title-box text-center">
                <h3
                  className="title-a"
                  style={{
                    fontFamily: "Indie Flower, cursive",
                    color: "white"
                  }}
                >
                  Locations:
                </h3>
                <div className="line-mf" />
              </div>
            </div>
          </div>
          <div className="row">
            {this.state.locations.map(location => {
              return <Location name={location.name} url={location.url} key={location.name} />;
            })}
          </div>
        </div>
      </section>
      </div>
    );
  }
}

export default Partners;
