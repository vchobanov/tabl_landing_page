import React, { Component } from "react";

class Location extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="col-md-4">
        <a href={this.props.url}>
          <div className="card card-blog">
            <div className="card-body">
              <h3 className="card-title"><b>{this.props.name}</b></h3>
              <p className="card-description" />
            </div>
          </div>
        </a>
      </div>
    );
  }
}

export default Location;
