import React, { Component } from "react";
import MarketingPage from "./components/pages/MarketingPage";
import Partners from "./components/pages/Partners";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
          <BrowserRouter>
            <div className="content">
              <Route exact path="/" component={MarketingPage} />
              <Route exact path="/locations" component={Partners} />
            </div>
          </BrowserRouter>
      </div>
    );
  }
}

export default App;
